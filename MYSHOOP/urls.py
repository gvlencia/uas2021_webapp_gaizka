from django.contrib import admin
from django.urls import path
from WEBSHOOP import views

urlpatterns = [
    path('admin/', admin.site.urls),
    path('product/', views.product, name='product'),
    path('product/create', views.ProdukCreate.as_view() ,name='productcreate'),
    path('product/update/<int:pk>', views.ProdukUpdate.as_view() ,name='productupdate'),
    path('product/delete/<int:pk>', views.ProdukDelete.as_view() ,name='productdelete'),
    path('stores/', views.store, name='stores'),
    path('stores/create', views.TokoCreate.as_view() ,name='storescreate'),
    path('stores/update/<int:pk>', views.TokoUpdate.as_view() ,name='storesupdate'),
    path('stores/delete/<int:pk>', views.TokoDelete.as_view() ,name='storesdelete'),
]
