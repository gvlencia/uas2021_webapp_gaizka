from django.contrib import admin
from .models import Toko, User, Produk

# Register your models here.
admin.site.register(Toko)
admin.site.register(Produk)
