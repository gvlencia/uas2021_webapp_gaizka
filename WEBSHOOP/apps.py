from django.apps import AppConfig


class WebshoopConfig(AppConfig):
    name = 'WEBSHOOP'
