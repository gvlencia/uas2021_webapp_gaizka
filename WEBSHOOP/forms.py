from django.forms import ModelForm
from django import forms
from .models import *

class TambahProduk(ModelForm):
    class Meta:
        model = Produk
        fields = '__all__'

class TambahToko(ModelForm):
    class Meta:
        model = Toko
        fields = '__all__'