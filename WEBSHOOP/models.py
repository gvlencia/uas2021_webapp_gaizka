from django.db import models
from django.contrib.auth.models import User

# Create your models here.
class Toko(models.Model):
    nama_toko = models.CharField(max_length=200)
    alamat = models.CharField(max_length=200)
    pemilik = models.ForeignKey(User, on_delete=models.CASCADE)
    def __str__(self):
        return self.nama_toko

class Produk(models.Model):
    nama = models.CharField(max_length=200)
    harga = models.CharField(max_length=200)
    image = models.ImageField(null=True, blank=True)
    keterangan = models.CharField(max_length=200)
    toko = models.ForeignKey(Toko, models.SET_NULL, blank=True, null=True, default=None)
    def __str__(self):
        return '{} ({})'.format(self.nama, self.toko)
