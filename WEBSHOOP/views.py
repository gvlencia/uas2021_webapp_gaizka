from django.shortcuts import render, redirect
from WEBSHOOP import models, forms
from django.urls import reverse_lazy
from django.views.generic.edit import CreateView, UpdateView, DeleteView

def product(request):
    produk = models.Produk.objects.all()
    context = {
        'produk': produk,

    }
    return render (request, 'produk.html', context)

def store(request):
    toko = models.Toko.objects.all()
    context = {
        'toko': toko,

    }
    return render (request, 'toko.html', context)


class ProdukCreate(CreateView):
    template_name = "produkadd.html"
    form_class = forms.TambahProduk

    def get_success_url(self):
        return reverse_lazy('product')

class ProdukUpdate(UpdateView):
    template_name = "produkadd.html"
    form_class = forms.TambahProduk
    model = models.Produk

    def get_success_url(self):
        return reverse_lazy('product')

class ProdukDelete(DeleteView):
    template_name = "produkdelete.html"
    form_class = forms.TambahProduk
    model = models.Produk

    def get_success_url(self):
        return reverse_lazy('product')

class TokoCreate(CreateView):
    template_name = "tokoadd.html"
    form_class = forms.TambahToko

    def get_success_url(self):
        return reverse_lazy('stores')

class TokoUpdate(UpdateView):
    template_name = "tokoadd.html"
    form_class = forms.TambahToko
    model = models.Toko

    def get_success_url(self):
        return reverse_lazy('stores')

class TokoDelete(DeleteView):
    template_name = "tokodelete.html"
    form_class = forms.TambahToko
    model = models.Toko

    def get_success_url(self):
        return reverse_lazy('stores')